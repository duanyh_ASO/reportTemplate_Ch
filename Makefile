#**********************************************************************************
#file name
fileName := template

#源码位置
#源码存在于多个位置时，以空格隔开即可
#sourceDir := 目录1 目录2
sourceDir := .

#源码文件
#sourceFiles := $(foreach texDir, $(sourceDir), $(wildcard $(texDir)/*.tex))
sourceFiles := $(sourceDir)/$(fileName).tex

#**********************************************************************************

EXEC := $(fileName).pdf

#终极目标
$(EXEC): $(sourceFiles)
#$(EXEC): manuscript.tex 
	xelatex $(sourceFiles)

	bibtex $(fileName).aux

	xelatex $(sourceFiles)
	xelatex $(sourceFiles)
	open $(fileName).pdf

#伪目标
.PHONY: clean

#清理编译产生的中间文件
clean:
	rm -f *.aux
	rm -f *.bak
	rm -f *.bbl
	rm -f *.bcf
	rm -f *.blg
	rm -f *.gz
	rm -f *.gz\(busy\)
	rm -f *.log
	rm -f *.out
	rm -f *.synctex
	rm -f *.toc
	rm -f *.xdv
	rm -f *.xml
	rm -f *.pdf
